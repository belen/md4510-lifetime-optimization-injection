import pjlsa
import jpype

import numpy as np

### rbac
import pyjapc

japc = pyjapc.PyJapc()
japc.rbacLogin()


cern=jpype.JPackage('cern')
java=jpype.JPackage('java')
DeviceRequestBuilder = cern.lsa.domain.devices.factory.DevicesRequestBuilder
TrimRequest          = cern.lsa.domain.settings.TrimRequest
ValueFactory         = cern.accsoft.commons.value.ValueFactory


lsa = pjlsa.LSAClient(server='gpn')
#lsa = pjlsa.LSAClient(server='lhc')
print("\n")

resident_bp = lsa.getResidentBeamProcess("POWERCONVERTERS")
resident_bp = "SQUEEZE-6.5TeV-ATS-1m-30cm-2018_V1@638_[END]" #for testing, non-resident 
print("Resident BP: ", resident_bp )

lsa_parameter_list = ( "LHCBEAM1/QH_TRIM",
                       "LHCBEAM1/QV_TRIM",
                       "LHCBEAM2/QH_TRIM",
                       "LHCBEAM2/QV_TRIM")
print("LSA parameters : ", lsa_parameter_list )

initial_parameter_trims = {}
for parameter in lsa_parameter_list:
    trims = lsa.getLastTrim(resident_bp, parameter)
    initial_parameter_trims[ parameter ] = trims.data

print(initial_parameter_trims)

delta_parameter_trims = {}
#final_parameter_trims = {}
for parameter in lsa_parameter_list:
    random_number = 0
    delta_parameter_trims[parameter] = random_number
    #final_parameter_trims[parameter] = initial_parameter_trims[ parameter ] + delta_parameter_trims[parameter]
    # use final_parameter_trims[parameter] to trim
    # how to trim all values at the same time?
    # wait 20 seconds
    # optional...
    # use initial_parameter_trims[parameter] to trim
    # wait 20 seconds


# Find parameter and context
parameterList = lsa._buildParameterList(lsa_parameter_list)
bp = lsa._getBeamProcess(resident_bp)

## TrimRequestBuilder
trb = cern.lsa.domain.settings.factory.TrimRequestBuilder()
trb.setDescription('Counter of the trim: ")
trb.setContext(bp)
trb.setRelative(True) # to trim in relative

# Values
deviceArray = jpype.java.util.ArrayList()
deviceArray.add(ValueFactory.createScalar(0.001))
deviceArray.add(ValueFactory.createScalar(0.001))
deviceArray.add(ValueFactory.createScalar(0.001))
deviceArray.add(ValueFactory.createScalar(0.001))
for i in range(len(parameterList)):
    trb.addValue(parameterList[i], bp, deviceArray[i] )

# Send Trim
trim_response = lsa._trimService.trimSettings( trb.build())

